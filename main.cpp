#include <Clove/Clove.hpp>

using namespace clv;

int main(){
	Log::init();

	auto platform = plt::createPlatformInstance(plt::getPlatformPreferedAPI()); //D3D is not quite as desireable
	auto& graphicsFactory = platform->getGraphicsFactory();

	auto mainWindow = platform->createWindow({"RayMarch", 1280, 720});
	mainWindow->setVSync(true);

	//Object creation
	std::string source_path = SOURCE_DIR;

	auto vertexShader = graphicsFactory.createShader({ gfx::ShaderStage::Vertex }, source_path + "/VertShader.hlsl");
	auto pixelShader = graphicsFactory.createShader({ gfx::ShaderStage::Pixel }, source_path + "/PixelShader.hlsl");

	auto pipeline = graphicsFactory.createPipelineObject();
	pipeline->setVertexShader(*vertexShader);
	pipeline->setPixelShader(*pixelShader);
	pipeline->setBlendState(true);
	pipeline->setCullMode(gfx::CullFace::Back, true);

	gfx::VertexBufferData vertices{ pipeline->getVertexLayout() };
	vertices.emplaceBack(mth::vec2f{ -1.0f, -1.0f }, mth::vec2f{ 0.0f, 0.0f });
	vertices.emplaceBack(mth::vec2f{ 1.0f, -1.0f }, mth::vec2f{ 1.0f, 0.0f });
	vertices.emplaceBack(mth::vec2f{ -1.0f,  1.0f }, mth::vec2f{ 0.0f, 1.0f });
	vertices.emplaceBack(mth::vec2f{ 1.0f,  1.0f }, mth::vec2f{ 1.0f, 1.0f });

	const std::vector<uint32> indices = {
		1, 3, 0,
		3, 2, 0
	};

	gfx::BufferDescriptor vbDesc{};
	vbDesc.elementSize	= vertices.getLayout().size();
	vbDesc.bufferSize	= vertices.sizeBytes();
	vbDesc.bufferType	= gfx::BufferType::VertexBuffer;
	vbDesc.bufferUsage	= gfx::BufferUsage::Default;
	auto vertexBuffer	= graphicsFactory.createBuffer(vbDesc, vertices.data());

	gfx::BufferDescriptor ibDesc{};
	ibDesc.elementSize	= sizeof(uint32);
	ibDesc.bufferSize	= indices.size() * sizeof(uint32);
	ibDesc.bufferType	= gfx::BufferType::IndexBuffer;
	ibDesc.bufferUsage	= gfx::BufferUsage::Default;
	auto indexBuffer	= graphicsFactory.createBuffer(ibDesc, indices.data());

	const mth::vec2f screenSize = mainWindow->getSize();

	gfx::BufferDescriptor resDesc{};
	resDesc.elementSize	= 0;
	resDesc.bufferSize	= 16; //D3D needs multiple of 16
	resDesc.bufferType	= gfx::BufferType::ShaderResourceBuffer;
	resDesc.bufferUsage	= gfx::BufferUsage::Default;
	auto resBuffer		= graphicsFactory.createBuffer(resDesc, &screenSize);

	mth::vec3f cameraPosition = { 0.0f, 1.0f, 0.0f };

	gfx::BufferDescriptor posDesc{};
	posDesc.elementSize	= 0;
	posDesc.bufferSize	= 16; //D3D needs multiple of 16
	posDesc.bufferType	= gfx::BufferType::ShaderResourceBuffer;
	posDesc.bufferUsage	= gfx::BufferUsage::Dynamic;
	auto posBuffer		= graphicsFactory.createBuffer(posDesc, &cameraPosition);

	float sec = 0.0f;

	gfx::BufferDescriptor tDesc{};
	tDesc.elementSize	= 0;
	tDesc.bufferSize	= 16;
	tDesc.bufferType	= gfx::BufferType::ShaderResourceBuffer;
	tDesc.bufferUsage	= gfx::BufferUsage::Dynamic;
	auto timeBuffer		= graphicsFactory.createBuffer(tDesc, &sec);

	auto commandBuffer = graphicsFactory.createCommandBuffer(mainWindow->getSurface());

	auto prevFrameTime = std::chrono::system_clock::now();
	const float speed = 5.0f;

	bool isWindowOpen = true;
	mainWindow->onWindowCloseDelegate.bind([&isWindowOpen](){ isWindowOpen = false; });

	while(isWindowOpen){
		auto currFrameTime = std::chrono::system_clock::now();
		std::chrono::duration<float> deltaSeonds = currFrameTime - prevFrameTime;
		prevFrameTime = currFrameTime;

		const float ds = deltaSeonds.count();

		sec += ds;
		timeBuffer->updateData(&sec);

		if(mainWindow->getKeyboard().isKeyPressed(Key::A)){
			cameraPosition.x -= speed * ds;
		}else if(mainWindow->getKeyboard().isKeyPressed(Key::D)){
			cameraPosition.x += speed * ds;
		}

		if(mainWindow->getKeyboard().isKeyPressed(Key::Space)){
			cameraPosition.y += speed * ds;
		}else if(mainWindow->getKeyboard().isKeyPressed(Key::Control_Left)){
			cameraPosition.y -= speed * ds;
		}

		if(mainWindow->getKeyboard().isKeyPressed(Key::W)){
			cameraPosition.z += speed * ds;
		}else if(mainWindow->getKeyboard().isKeyPressed(Key::S)){
			cameraPosition.z -= speed * ds;
		}

		posBuffer->updateData(&cameraPosition);

		mainWindow->beginFrame();

		commandBuffer->beginEncoding();

		commandBuffer->setViewport({ 0, 0, static_cast<int32>(screenSize.x), static_cast<int32>(screenSize.y) });
		commandBuffer->bindPipelineObject(*pipeline);
		commandBuffer->bindVertexBuffer(*vertexBuffer, static_cast<uint32>(pipeline->getVertexLayout().size()));
		commandBuffer->bindIndexBuffer(*indexBuffer);
		commandBuffer->bindShaderResourceBuffer(*resBuffer, gfx::ShaderStage::Pixel, 1);
		commandBuffer->bindShaderResourceBuffer(*posBuffer, gfx::ShaderStage::Pixel, 2);
		commandBuffer->bindShaderResourceBuffer(*timeBuffer, gfx::ShaderStage::Pixel, 3);

		commandBuffer->drawIndexed(indices.size());

		commandBuffer->endEncoding();

		mainWindow->endFrame();

		if(mainWindow->getKeyboard().isKeyPressed(Key::Escape)){
			break;
		}
	}

	return 0;
}
