#define MAX_STEPS 1000
#define MAX_DIST 1000.0f
#define SURF_DIST 0.1f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

float2x2 rotate(float angle){
	float s = sin(angle);
	float c = cos(angle);
	return float2x2(c, -s, s, c);
}

//Buffers
cbuffer resolutionBuffer : register(b1){
	float2 resolution;
};

cbuffer cameraPositionBuffer : register(b2){
	float3 cameraPosition;
};

cbuffer cameraPositionBuffer : register(b3){
	float time;
};

//Distance estimaters
float planeDistance(float3 pos){
	return pos.y + 1;	
}

float sphereDistance(float3 pos){
	float sphereRadius = 1.0f;
	return length(pos) - sphereRadius;
}

float tetrahedronDistance(float3 pos){
	float scale = 2.0f;
	float spacing = 30.0f;
	int iterations = 50;
	for(int i = 0; i < iterations; i++){
		pos.zy = mul(pos.zy, rotate(time * 0.2f));
		
		if(pos.x + pos.y < 0) { pos.xy = -pos.yx; }
		if(pos.x + pos.z < 0) { pos.xz = -pos.zx; }
		if(pos.y + pos.z < 0) { pos.yz = -pos.zy; }
		
		pos.x = abs(pos.x);
		pos.y = abs(pos.y);
		pos.z = abs(pos.z);
			
		pos = pos * scale - spacing * (scale - 1.0f);
		
		
		
		//pos.xy = mul(pos.xy, rotate(0.8f));
		
	}
	return length(pos) * pow(scale, -float(iterations));
}

float getDistance(float3 pos){
	/*float pd = planeDistance(pos);
	
	//Warp some space
	pos.xz = goodmod(pos.xz, 8.0f) - float3(4.0f, 4.0f, 4.0f);

	float sd = sphereDistance(pos);

	return sd;*/
	
	return tetrahedronDistance(pos);
}

//Marching
struct MarchResult{
	float prevDistance;
	float totalDistance;
	int steps;
};

MarchResult march(float3 origin, float3 direction){
	MarchResult result;

    for(int i = 0; i < MAX_STEPS; ++i){
		result.steps = i;
		
		float3 pos = origin + direction * result.totalDistance;
    	float distance = getDistance(pos);

		result.totalDistance += distance;
		result.prevDistance = distance;

		if(result.totalDistance > MAX_DIST || distance < SURF_DIST){
			break;
		}
	}

    return result;
}

//Lighting
float3 getNormal(float3 pos){
	float distance = getDistance(pos);
	float2 epsilon = float2(0.01f, 0.0f);

	float3 normal = distance - float3(getDistance(pos - epsilon.xyy), getDistance(pos - epsilon.yxy), getDistance(pos - epsilon.yyx));

	return normalize(normal);
}

float calcDiffusePointLight(float3 pos){
	float3 lightPos = float3(0, 5, 0);
	float3 dirToLight = normalize(lightPos - pos);
	
	float3 posNormal = getNormal(pos);
	
	float lightStrength = clamp(dot(posNormal, dirToLight), 0.0f, 1.0f);
	
	float distToLight = march(pos + posNormal * SURF_DIST * 2.0f, dirToLight).totalDistance;
	if(distToLight < length(lightPos - pos)){
		lightStrength *= 0.1f;	
	}
	
	return lightStrength;
}

float calcDiffuseDirectionalLight(float3 pos){
	float3 dirToLight = normalize(float3(0.0f, 1.0f, -1.0f));
	
	float3 posNormal = getNormal(pos);
	
	float lightStrength = clamp(dot(posNormal, dirToLight), 0.0f, 1.0f);

	float distToLight = march(pos + posNormal * SURF_DIST * 2.0f, dirToLight).totalDistance;
	if(distToLight < MAX_DIST){
		lightStrength *= 0.1f;	
	}
	
	return lightStrength;
}

float4 main(float2 texCoord : TexCoord) : SV_Target{
	float2 pixelCoord = float2(texCoord.x * resolution.x, texCoord.y * resolution.y);
    float2 uv = (pixelCoord - (0.5f * resolution)) / resolution.y;

    float3 rayOrigin = cameraPosition;
    float3 rayDirection = normalize(float3(uv.x, uv.y, 1.0f));

    MarchResult result = march(rayOrigin, rayDirection);

    float3 currentPos = rayOrigin + rayDirection * result.totalDistance;
	float diffLight = calcDiffuseDirectionalLight(currentPos);

	float disPercent =  1.0f - (result.totalDistance / (MAX_DIST * 0.3f));
	
	float3 baseColour = float3(1.0f, 0.0f, 1.0f);

	float3 finalColour = float3(0.0f, 0.0f, 0.0f);
	if(result.prevDistance < SURF_DIST){
		finalColour = float3(disPercent, disPercent * 0.15f, 1.0f - disPercent) * diffLight;
	}else{
		finalColour = float3(0.0f, 1.0f, 1.0f);
	}

	return float4(finalColour, 1.0f);
}
